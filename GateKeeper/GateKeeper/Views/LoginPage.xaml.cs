﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GateKeeper.Views
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
	{

		public LoginPage ()
		{
			InitializeComponent();

            Logo.Source = ImageSource.FromFile("Logo.png");

        }   

        private void Login_btn(object sender, EventArgs e)
        {
            Navigation.PopAsync();
            Navigation.PushAsync(new MainMasterDetailPage());
           
        }
    }
}