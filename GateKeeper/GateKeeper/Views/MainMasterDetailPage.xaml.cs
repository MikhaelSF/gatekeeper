﻿using GateKeeper.MenuItems;
using GateKeeper.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GateKeeper.Views
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainMasterDetailPage : MasterDetailPage
	{
		public MainMasterDetailPage ()
		{
			InitializeComponent();
            BindingContext = new MainMasterDetailPageViewModel();
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(UsersPanel)));
        }

        void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (MainMasterDetailPageItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;
        }
	}
}