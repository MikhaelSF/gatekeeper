﻿using GateKeeper.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GateKeeper.Views
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActiveUsersListViewPage : ContentPage
    {
        public ActiveUsersListViewPage()
        {
            InitializeComponent();
            BindingContext = new ActiveUsersListViewPageViewModel();
        }

        void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
             /*   if (e.Item == null)
                    return;

                  await DisplayAlert("Item Tapped", "An item was tapped.", "OK");

                  //Deselect Item
                  ((ListView)sender).SelectedItem = null;
            
            */
        }
    }
}
