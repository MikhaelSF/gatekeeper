﻿using GateKeeper.Models;
using GateKeeper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GateKeeper.Views
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventsPanel : ContentPage
    {
        Fab fab = new Fab();
        Fab addEventFab = new Fab();
        Fab addProcessFab = new Fab();
        Fab addExceptionFab = new Fab();

        List<Fab> fabs { get; set; } = new List<Fab>();

        public EventsPanel ()
		{
			InitializeComponent();
            BindingContext = new EventsPanelViewModel();

            fab = new Fab()
            {
                ImageName = "plus.png",
                ColorNormal = Color.FromHex("ff3498db"),
                ColorPressed = Color.Black,
                ColorRipple = Color.FromHex("ff3498db"),
                HasShadow = true
            };

            addEventFab = new Fab()
            {
                ImageName = "UsersPanelMenuIcon2.png",
                ColorNormal = Color.FromHex("004D40"),
                ColorPressed = Color.Aqua,
                ColorRipple = Color.FromHex("00695C"),
                HasShadow = true
            };

            addProcessFab = new Fab()
            {
                ImageName = "Process.png",
                ColorNormal = Color.FromHex("004D40"),
                ColorPressed = Color.Aqua,
                ColorRipple = Color.FromHex("00695C"),
                HasShadow = true
            };

            addExceptionFab = new Fab()
            {
                ImageName = "Error.png",
                ColorNormal = Color.FromHex("004D40"),
                ColorPressed = Color.Aqua,
                ColorRipple = Color.FromHex("00695C"),
                HasShadow = true
            };

            ListItems.IsVisible = false;

            UL.IsVisible = true;

            Option.IsVisible = false;

            OptionProcess.IsVisible = false;

            OptionException.IsVisible = false;

            AddEventPanel.IsVisible = false;

            AddSuccessfully.IsVisible = false;

            AddItemsTo();

            GoAddEventPanel();

            OptionProcessClicked();

            //SaveEvent();
        }

        void AddItemsTo()
        {
            ListItems.Children.Add(addExceptionFab);
            ListItems.Children.Add(addProcessFab);
            ListItems.Children.Add(addEventFab);

            FAB.Children.Add(fab);

            fab.Clicked += (sender, e) =>
            {
                fab.RelRotateTo(180, 1000, Easing.BounceOut);

                if (fab.ImageName.Equals("plus.png") && ListItems.IsVisible.Equals(false) && Option.IsVisible.Equals(false))
                {
                    fab.ImageName = "cancel.png";
                    ListItems.IsVisible = true;
                    OptionException.IsVisible = true;
                    OptionProcess.IsVisible = true;
                    Option.IsVisible = true;
                    ListItems.TranslateTo(0, -120, 500, Easing.BounceIn);
                    Option.TranslateTo(0, -33, 500, Easing.BounceIn);
                    OptionProcess.TranslateTo(0, -31, 500, Easing.BounceIn);
                    OptionException.TranslateTo(0, -8, 500, Easing.BounceIn);
                    UL.Opacity = 0.5;
                    UL.IsEnabled = false;
                }
                else
                {
                    fab.ImageName = "plus.png";
                    ListItems.IsVisible = false;
                    UL.Opacity = 1;
                    Option.IsVisible = false;
                    OptionProcess.IsVisible = false;
                    OptionException.IsVisible = false;
                    UL.IsEnabled = true;
                    AddEventPanel.IsVisible = false;
                }
            };
        }

        void GoAddEventPanel()
        {
            addEventFab.Clicked += (sender, e) =>
            {
                fab.RotateTo(180, 500, Easing.BounceIn);

                if (fab.ImageName.Equals("cancel.png") && ListItems.IsVisible.Equals(true) && Option.IsVisible.Equals(true) && UL.Opacity.Equals(0.5))
                {
                    fab.RelRotateTo(180, 500, Easing.BounceOut);
                    AddEventPanel.IsVisible = true;
                    AddEventPanel.TranslateTo(0, -100, 500, Easing.BounceIn);
                    Option.IsVisible = false;
                    OptionProcess.IsVisible = false;
                    OptionException.IsVisible = false;
                    ListItems.IsVisible = false;
                    fab.ImageName = "cancel.png";
                    UL.IsEnabled = false;
                }
                else
                {
                    fab.ImageName = "plus.png";
                    ListItems.IsVisible = false;
                    UL.Opacity = 1;
                    UL.IsEnabled = true;
                    Option.IsVisible = false;
                    OptionProcess.IsVisible = false;
                    OptionException.IsVisible = false;
                }
            };
        }

        void Success_Clicked()
        {

            fab.RelRotateTo(180, 500, Easing.BounceIn);
            AddSuccessfully.IsVisible = false;
            ListItems.IsVisible = false;
            UL.Opacity = 1;
            UL.IsVisible = true;
            UL.IsEnabled = true;
            Option.IsVisible = false;
            OptionProcess.IsVisible = false;
            AddEventPanel.IsVisible = false;
            Success.IsVisible = false;
            fab.IsVisible = true;

        }

        void OptionProcessClicked()
        {
            addEventFab.Clicked += (sender, e) =>
            {
                AddEventPanel.IsVisible = true;
                UL.IsEnabled = false;
                UL.Opacity = 0.5;
                Option.IsVisible = false;
                OptionException.IsVisible = false;
                OptionProcess.IsVisible = false;
            };
        }

        void OptionEventClicked()
        {
            
        }

        void DateSelected()
        {
            EventDate.DateSelected += (sender, e) =>
            {

            };
        }

        void HourSelected()
        {
            EventHour.PropertyChanged += EventHour_PropertyChanged; 
        }

        private void EventHour_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "time")
            {

            }
        }

        private void OnTap(View arg1, object arg2)
        {
            Success_Clicked();
        }
    }
}