﻿using GateKeeper.Models;
using GateKeeper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GateKeeper.Views
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UsersPanel : ContentPage
	{
        Fab fab = new Fab();
        Fab addUserFab = new Fab();
       
        public UsersPanel ()
		{
			InitializeComponent();
            BindingContext = new UsersPanelViewModel();

            fab = new Fab()
            {
                ImageName = "plus.png",
                ColorNormal = Color.FromHex("ff3498db"),
                ColorPressed = Color.Black,
                ColorRipple = Color.FromHex("ff3498db"),
                HasShadow = true
            };

            addUserFab = new Fab() {
                ImageName = "UsersPanelMenuIcon2.png",
                ColorNormal = Color.FromHex("004D40"),
                ColorPressed = Color.Aqua,
                ColorRipple = Color.FromHex("00695C"),
                HasShadow = true
            };

            ListItems.IsVisible = false;

            UL.IsVisible = true;
            
            Option.IsVisible = false;

            AddUserPanel.IsVisible = false;

            AddSuccessfully.IsVisible = false;

            //Success.IsVisible = false;

            AddItemsTo();

            GoAddUserPanel();

            SaveUser();

        }

        void AddItemsTo()
        {
            ListItems.Children.Add(addUserFab);

            FAB.Children.Add(fab);

            fab.Clicked += (sender, e) =>
            {
                fab.RelRotateTo(180, 1000, Easing.BounceOut);
                
                if (fab.ImageName.Equals("plus.png") && ListItems.IsVisible.Equals(false) && Option.IsVisible.Equals(false))
                {
                    fab.ImageName = "cancel.png";
                    ListItems.IsVisible = true;
                    ListItems.TranslateTo(0, -85, 500, Easing.BounceIn);
                    Option.TranslateTo(0, -30, 500, Easing.BounceIn);
                    UL.Opacity = 0.5;
                    Option.IsVisible = true;
                    UL.IsEnabled = false;
                }
                else
                {
                    fab.ImageName = "plus.png";
                    ListItems.IsVisible = false;
                    UL.Opacity = 1;
                    Option.IsVisible = false;
                    UL.IsEnabled = true;
                    AddUserPanel.IsVisible = false;
                }
            };
        }

        void GoAddUserPanel()
        { 

            addUserFab.Clicked += (sender, e) =>
            {
                fab.RotateTo(180, 500, Easing.BounceIn);
               
                if (fab.ImageName.Equals("cancel.png") && ListItems.IsVisible.Equals(true) && Option.IsVisible.Equals(true) && UL.Opacity.Equals(0.5))
                {
                    fab.RelRotateTo(180, 500, Easing.BounceOut);
                    AddUserPanel.IsVisible = true;
                    AddUserPanel.TranslateTo(0, -20, 500, Easing.BounceIn);
                    Option.IsVisible = false;
                    ListItems.IsVisible = false;
                    fab.ImageName = "cancel.png";
                    UL.IsEnabled = false;
                }
                else
                {
                    fab.ImageName = "plus.png";
                    ListItems.IsVisible = false;
                    UL.Opacity = 1;
                    UL.IsEnabled = true;
                    Option.IsVisible = false;
                }
            };
        }

        void SaveUser()
        {
            Save.Clicked += (sender, e) =>
            {
                AddSuccessfully.IsVisible = true;
                ListItems.IsVisible = false;
                UL.Opacity = 0.5;
                UL.IsVisible = true;
                UL.IsEnabled = false;
                Option.IsVisible = false;
                AddUserPanel.IsVisible = false;
                Success.IsVisible = true;
                Success.Source = ImageSource.FromFile("AddUserSuccess.png");
#pragma warning disable CS0618 // Type or member is obsolete
                Success.GestureRecognizers.Add(new TapGestureRecognizer(OnTap));
#pragma warning restore CS0618 // Type or member is obsolete
                fab.IsVisible = false;
                //Success_Clicked();
            };
        }

        void Success_Clicked() {

            fab.RelRotateTo(180, 500, Easing.BounceIn);
            AddSuccessfully.IsVisible = false;
            ListItems.IsVisible = false;
            UL.Opacity = 1;
            UL.IsVisible = true;
            UL.IsEnabled = true;
            Option.IsVisible = false;
            AddUserPanel.IsVisible = false;
            Success.IsVisible = false;
            fab.IsVisible = true;
            
        }

        private void OnTap(View arg1, object arg2)
        {
            Success_Clicked();
        }
    }
}