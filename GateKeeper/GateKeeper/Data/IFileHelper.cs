﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GateKeeper.Data
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
