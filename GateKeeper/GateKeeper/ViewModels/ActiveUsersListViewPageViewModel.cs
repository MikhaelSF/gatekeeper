﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GateKeeper.Models;

namespace GateKeeper.ViewModels
{
    public class ActiveUsersListViewPageViewModel
    {
        
        public ObservableCollection<User> UsersActivityList{ get; set; } = new ObservableCollection<User>();

        public ActiveUsersListViewPageViewModel()
        {
            generateUsers();
            addUsersToUsersActivityList();
        }

        async void generateUsers()
        {
            for (int i = 0; i < 15; i++)
            {
                var user = new User()
                {
                    Name = "User Number " + i,
                    ProfileImage = "ProfileImage.png",
                    Time = DateTime.Now,
                    Access = true
                };

                await App.Database.SaveUserAsync(user);
            }
        }

        async void addUsersToUsersActivityList()
        {
            // Getting all users.
            List<User> users = await App.Database.GetUsersAsync();

            foreach (var user in users)
            {
                UsersActivityList.Add(user);
            }

            /*foreach (var user in users)
            {
                var date = DateTime.Now;
                var checkInTime = date.AddHours(9);
                var deapertureTime = date.AddHours(2);
                UsersActivityList.Add(user);
            }*/
        }
    }
}
