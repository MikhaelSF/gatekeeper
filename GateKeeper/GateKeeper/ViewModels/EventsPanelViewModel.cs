﻿using System;
using System.Collections.Generic;
using System.Text;
using GateKeeper.Models;

namespace GateKeeper.ViewModels
{
    public class EventsPanelViewModel
    {
        public List<Event> Event_List { get; set; } = new List<Event>();

        public EventsPanelViewModel()
        {
            AddEventsToList();
        }

        void AddEventsToList()
        {
            Event_List.Add(new Event()
            {
                Date = DateTime.Now.Date,
                Hour = DateTime.Now.Hour,
                Description = "Grant me access to the zone number 4",
                Caption = "Access"
            });

            Event_List.Add(new Event()
            {
                Date = DateTime.Now.Date,
                Hour = DateTime.Now.Hour,
                Description = "Grant me access to the zone number 7",
                Caption = "Access"
            });
        }
    }
}
