﻿using GateKeeper.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GateKeeper.ViewModels
{
    //Todo lo puse en inglés y utilizé tu método para no
    //Reinventar la rueda xD
    public class UsersPanelViewModel {

        public List<User> UsersList { get; set; } = new List<User>();

        public UsersPanelViewModel()
        {
            AddUsersToList();
        }

        void AddUsersToList()
        {

            UsersList.Add(new User() {
                Name = "Sergio Joel",
                Access = true,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });
            
            UsersList.Add(new User()
            {
                Name = "Mikhael",
                Access = true,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "Daniel Guerrero",
                Access = false,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "Luis Santiago",
                Access = false,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "Ian",
                Access = true,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "JKCNAK",
                Access = false,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "AKSJCK",
                Access = false,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "JKCNAK",
                Access = false,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });

            UsersList.Add(new User()
            {
                Name = "AKSJCK",
                Access = false,
                ProfileImage = "icon.png",
                Time = DateTime.UtcNow
            });
        }
    }
}
