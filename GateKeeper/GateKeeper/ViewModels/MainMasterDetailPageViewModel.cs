﻿using GateKeeper.MenuItems;
using System;
using System.Collections.Generic;
using System.Text;
using GateKeeper.Views;

namespace GateKeeper.ViewModels
{
    public class MainMasterDetailPageViewModel
    {
        public List<MainMasterDetailPageItem> MenuList { get; set; } = new List<MainMasterDetailPageItem>();

        public MainMasterDetailPageViewModel()
        {
            addNavigationItemsToMenuList();
        }

        void addNavigationItemsToMenuList()
        {
            MenuList.Add(new MainMasterDetailPageItem()
            {
                Title = "Users Panel",
                Icon = "UsersPanelMenuIcon.png",
                TargetType = typeof(UsersPanel)
            });

            MenuList.Add(new MainMasterDetailPageItem()
            {
                Title = "Event",
                Icon = "icon.png",
                TargetType = typeof(EventsPanel)
            });

            MenuList.Add(new MainMasterDetailPageItem()
            {
                Title = "Reports",
                Icon = "TestingPage2MenuIcon.png",
                TargetType = typeof(ReportsTabbedPage)
            });
        }
    }
}
