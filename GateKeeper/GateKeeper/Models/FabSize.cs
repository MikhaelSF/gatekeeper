﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace GateKeeper.Models
{
    public enum FabSize
    {
        /// <summary>
        /// Normal size
        /// </summary>
        Normal,
        /// <summary>
        /// Mini size
        /// </summary>
        Mini
    }
}
