﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GateKeeper.Models
{
    public class Zone
    {
        public string Name { get; set; }
        public bool AccesStatus { get; set; }
        public string AccesControlUUID { get; set; }
        public string AntennaInRange { get; set; }

        public Zone()
        { }
    }
}
