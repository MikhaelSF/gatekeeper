﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GateKeeper.Models
{
    /**
     *En esta clase se definen los parametros para el login del sistema.. 
     * @Param <<String userEmail>>
     * @Param <<String password>>
     * @Param <<Enum role>>
     */
 
    class Login
    {
        private string userEmail { get; set; }

        public string password { get; set; }

        public Enum role { get; set; }

        public Login()
        { }
    }
}
