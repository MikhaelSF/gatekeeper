﻿using System;
using SQLite;
using Xamarin.Forms;

namespace GateKeeper.Models
{
    public class User
    {
        public User()
        { }

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Name { get; set; }

        public DateTime Time { get; set; }

        private Boolean access = false;
        public Boolean Access
        {
            get => access;

            set
            {
                access = value;
                if (access == true)
                {
                    AccesStatus = "Access Granted";
                }
                else
                {
                    AccesStatus = "Access Denied";
                }
            }
        }
        private string profileSource;
        public string ProfileImage
        {
            get => profileSource;
            set => profileSource = value;
        }

        public string AccesStatus { get; set; }
    }
}
