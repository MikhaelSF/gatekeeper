﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GateKeeper.Models
{
    public enum Roles
    {
        /**
         *  Enum para saber el tipo de usuario que ha ingresad
         */ 
        admin,
        user
    }
}
