﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace GateKeeper.Models
{
    public class Event
    {
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        private DateTime date;
        public DateTime Date
        {
            get => date;
            set => date = value;
        }

        private int hour;
        public int Hour
        {
            get => hour;
            set => hour = value;
        }

        private string caption;
        public string Caption
        {
            get => caption;
            set => caption = value;
        }

        private string description;
        public string Description
        {
            get => description;
            set => description = value;
        }

        private bool allowed;
        public bool Allowed
        {
            get => allowed;
            set => allowed = value;
        }

        private bool denied;
        public bool Denied
        {
            get => denied;
            set => denied = value;
        }
    }
}
