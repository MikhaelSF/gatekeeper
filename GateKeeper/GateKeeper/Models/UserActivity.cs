﻿using System;

namespace GateKeeper.Models
{
    public class UserActivity : User
    {
        public DateTime MomentOfDeparture;
        public DateTime MomentOfEntry;
        public String Activity; 

        public UserActivity()
        { }
    }
}
