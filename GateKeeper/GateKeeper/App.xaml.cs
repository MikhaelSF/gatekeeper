﻿using System;
using GateKeeper.Data;
using GateKeeper.Views;
using Xamarin.Forms;

namespace GateKeeper
{
	public partial class App : Application
	{
        static UserDatabase database;
        public static UserDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new UserDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("Gatekeeper.db3"));
                }
                return database;
            }
        }
        public App ()
		{
			InitializeComponent();
            MainPage = new NavigationPage(new LoginPage());
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
       
        //private Fill_Current_User_Data
	}
}
