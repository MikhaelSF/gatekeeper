﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using GateKeeper.Data;

[assembly:Xamarin.Forms.Dependency(typeof(GateKeeper.Droid.FileHelper))]
namespace GateKeeper.Droid
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}