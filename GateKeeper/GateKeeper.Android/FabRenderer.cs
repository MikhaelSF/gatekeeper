﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android.AppCompat;
using GateKeeper.Models;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System.IO;
using GateKeeper.Droid;
using Refractored.Fab;

[assembly: ExportRenderer(typeof(Fab), typeof(FabRenderer))]
namespace GateKeeper.Droid
{
    #pragma warning disable CS0618 // Type or member is obsolete
    public class FabRenderer : Xamarin.Forms.Platform.Android.ViewRenderer<Fab, FrameLayout>
    {
        /// <summary>
        /// Used for registration with dependency service
        /// </summary>
        public static void Init()
        {
            var temp = DateTime.Now;
        }

        private const int MARGIN_DIPS = 16;
        private const int FAB_HEIGHT_NORMAL = 56;
        private const int FAB_HEIGHT_MINI = 40;
        private const int FAB_FRAME_HEIGHT_WITH_PADDING = (MARGIN_DIPS * 2) + FAB_HEIGHT_NORMAL;
        private const int FAB_FRAME_WIDTH_WITH_PADDING = (MARGIN_DIPS * 2) + FAB_HEIGHT_NORMAL;
        private const int FAB_MINI_FRAME_HEIGHT_WITH_PADDING = (MARGIN_DIPS * 2) + FAB_HEIGHT_MINI;
        private const int FAB_MINI_FRAME_WIDTH_WITH_PADDING = (MARGIN_DIPS * 2) + FAB_HEIGHT_MINI;
        private readonly Context context;
        private readonly FloatingActionButton fab;

#pragma warning disable CS0618 // Type or member is obsolete
                              /// <summary>
                              /// Construtor
                              /// </summary>
        public FabRenderer()
        {
            context = Xamarin.Forms.Forms.Context;

            float d = context.Resources.DisplayMetrics.Density;
            var margin = (int)(MARGIN_DIPS * d); // margin in pixels

            fab = new FloatingActionButton(context);
            var lp = new FrameLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent);
            lp.Gravity = GravityFlags.CenterVertical | GravityFlags.CenterHorizontal;
            lp.LeftMargin = margin;
            lp.TopMargin = margin;
            lp.BottomMargin = margin;
            lp.RightMargin = margin;
            fab.LayoutParameters = lp;
        }
#pragma warning restore CS0618 // Type or member is obsolete

        /// <summary>
        /// Element Changed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnElementChanged(ElementChangedEventArgs<Fab> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
                return;

            if (e.OldElement != null)
                e.OldElement.PropertyChanged -= HandlePropertyChanged;

            if (this.Element != null)
            {
                //UpdateContent ();
                this.Element.PropertyChanged += HandlePropertyChanged;
            }

            Element.Show = Show;
            Element.Hide = Hide;

            SetFabImage(Element.ImageName);
            SetFabSize((Refractored.Fab.FabSize)Element.Size);

            fab.ColorNormal = Element.ColorNormal.ToAndroid();
            fab.ColorPressed = Element.ColorPressed.ToAndroid();
            fab.ColorRipple = Element.ColorRipple.ToAndroid();
            fab.HasShadow = Element.HasShadow;
            fab.Click += Fab_Click;

            var frame = new FrameLayout(context);
            frame.RemoveAllViews();
            frame.AddView(fab);

            SetNativeControl(frame);
        }

        /// <summary>
        /// Show
        /// </summary>
        /// <param name="animate"></param>
        public void Show(bool animate = true) =>
            fab?.Show(animate);


        /// <summary>
        /// Hide!
        /// </summary>
        /// <param name="animate"></param>
        public void Hide(bool animate = true) =>
            fab?.Hide(animate);


        void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Content")
            {
                Tracker.UpdateLayout();
            }
            else if (e.PropertyName == Fab.ColorNormalProperty.PropertyName)
            {
                fab.ColorNormal = Element.ColorNormal.ToAndroid();
            }
            else if (e.PropertyName == Fab.ColorPressedProperty.PropertyName)
            {
                fab.ColorPressed = Element.ColorPressed.ToAndroid();
            }
            else if (e.PropertyName == Fab.ColorRippleProperty.PropertyName)
            {
                fab.ColorRipple = Element.ColorRipple.ToAndroid();
            }
            else if (e.PropertyName == Fab.ImageNameProperty.PropertyName)
            {
                SetFabImage(Element.ImageName);
            }
            else if (e.PropertyName == Fab.SizeProperty.PropertyName)
            {
                SetFabSize((Refractored.Fab.FabSize)Element.Size);
            }
            else if (e.PropertyName == Fab.HasShadowProperty.PropertyName)
            {
                fab.HasShadow = Element.HasShadow;
            }
        }

        void SetFabImage(string imageName)
        {
            if (!string.IsNullOrWhiteSpace(imageName))
            {
                try
                {
                    var drawableNameWithoutExtension = Path.GetFileNameWithoutExtension(imageName).ToLower();
                    var resources = context.Resources;
                    var imageResourceName = resources.GetIdentifier(drawableNameWithoutExtension, "drawable", context.PackageName);
                    fab.SetImageBitmap(Android.Graphics.BitmapFactory.DecodeResource(context.Resources, imageResourceName));
                }
                catch (Exception ex)
                {
                    throw new FileNotFoundException("There was no Android Drawable by that name.", ex);
                }
            }
        }

        void SetFabSize(Refractored.Fab.FabSize size)
        {
            if (size == Refractored.Fab.FabSize.Mini)
            {
                fab.Size = Refractored.Fab.FabSize.Mini;
                Element.WidthRequest = FAB_MINI_FRAME_WIDTH_WITH_PADDING;
                Element.HeightRequest = FAB_MINI_FRAME_HEIGHT_WITH_PADDING;
            }
            else
            {
                fab.Size = Refractored.Fab.FabSize.Normal;
                Element.WidthRequest = FAB_FRAME_WIDTH_WITH_PADDING;
                Element.HeightRequest = FAB_FRAME_HEIGHT_WITH_PADDING;
            }
        }

        void Fab_Click(object sender, EventArgs e)
        {
            Element?.Clicked?.Invoke(sender, e);

            if (Element?.Command?.CanExecute(Element?.CommandParameter) ?? false)
            {
                Element.Command.Execute(Element?.CommandParameter);
            }
        }
        #pragma warning restore CS0618 // Type or member is obsolete
    }
}